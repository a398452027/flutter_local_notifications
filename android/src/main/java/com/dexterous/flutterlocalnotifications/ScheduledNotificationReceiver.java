package com.dexterous.flutterlocalnotifications;

import android.app.ActivityManager;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Keep;
import androidx.core.app.NotificationManagerCompat;

import com.dexterous.flutterlocalnotifications.models.NotificationDetails;
import com.dexterous.flutterlocalnotifications.utils.StringUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/** Created by michaelbui on 24/3/18. */
@Keep
public class ScheduledNotificationReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(final Context context, Intent intent) {
    String notificationDetailsJson =
            intent.getStringExtra(FlutterLocalNotificationsPlugin.NOTIFICATION_DETAILS);
    if (StringUtils.isNullOrEmpty(notificationDetailsJson)) {
      // This logic is needed for apps that used the plugin prior to 0.3.4
      Notification notification = intent.getParcelableExtra("notification");
      notification.when = System.currentTimeMillis();
      int notificationId = intent.getIntExtra("notification_id", 0);
      NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
      notificationManager.notify(notificationId, notification);
      boolean repeat = intent.getBooleanExtra("repeat", false);
      if (!repeat) {
        FlutterLocalNotificationsPlugin.removeNotificationFromCache(context, notificationId);
      }
    } else {
      Gson gson = FlutterLocalNotificationsPlugin.buildGson();
      Type type = new TypeToken<NotificationDetails>() {}.getType();
      NotificationDetails notificationDetails = gson.fromJson(notificationDetailsJson, type);

      if (appOnForeground(context)) {
        FlutterLocalNotificationsPlugin.notificationOnActive(notificationDetails);
      } else {
        FlutterLocalNotificationsPlugin.showNotification(context, notificationDetails);
      }
      if (notificationDetails.scheduledNotificationRepeatFrequency != null) {
        FlutterLocalNotificationsPlugin.zonedScheduleNextNotification(context, notificationDetails);
      } else if (notificationDetails.matchDateTimeComponents != null) {
        FlutterLocalNotificationsPlugin.zonedScheduleNextNotificationMatchingDateComponents(
                context, notificationDetails);
      } else if (notificationDetails.repeatInterval != null) {
        FlutterLocalNotificationsPlugin.scheduleNextRepeatingNotification(
                context, notificationDetails);
      } else {
        FlutterLocalNotificationsPlugin.removeNotificationFromCache(
                context, notificationDetails.id);
      }
    }
  }
  private boolean appOnForeground(Context context) {
    ActivityManager mActivityManager =
            (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    List<ActivityManager.RunningAppProcessInfo> appProcesses = mActivityManager.getRunningAppProcesses();

    if (mActivityManager==null||appProcesses == null){
      Log.d("FlutterLocal", "后台  ");
      return false;
    }

    for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
      if (appProcess.processName.equals(context.getPackageName()) && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
        Log.d("FlutterLocal", "前台  ");
        return true;
      }
    }
    Log.d("FlutterLocal", "后台  ");
    return false;
  }

  public String getForegroundActivity(Context mContext) {
    ActivityManager mActivityManager =
            (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
    if (mActivityManager.getRunningTasks(1) == null) {
      return null;
    }
    List<ActivityManager.RunningTaskInfo> tasks=mActivityManager.getRunningTasks(1);
    if(tasks.size()==0){
      return null;
    }
    ActivityManager.RunningTaskInfo mRunningTask =
            tasks.get(0);
    if (mRunningTask == null) {
      return null;
    }

    String pkgName = mRunningTask.topActivity.getPackageName();
    //String activityName =  mRunningTask.topActivity.getClassName();

    return pkgName;
  }
}
